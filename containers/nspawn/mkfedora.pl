#!/usr/bin/env perl

use strict;
use warnings;
use utf8;
use 5.032;

# ------ Library Import Start ------
use Getopt::Long qw(:config ignore_case);
use Pod::Usage;
use Number::Bytes::Human qw(parse_bytes);
# ------ Library Import End --------

# ------ Option Variables Start ------
my $help = '';
my $man = '';
my $force_run = 1;
my $container_path = '/var/lib/machines';
my $release_version = 0;
my $clone = '';
my $ephemeral = 1;
my $disk_size = '10G';
# ------ Option Variables End --------

# ------ Local Variables Start ------
my $network_file_path = "etc/systemd/network";
my $network_file_name = "20-wired.network";
my $nspawn_file_path = "/etc/systemd/nspawn";
# ------ Local Variables End --------

# ------ Local Files Start ------
# Systemd-networkd network file.
my $network_file = <<"EOF";
[Match]
Name=*

[Network]
DHCP=yes
LinkLocalAddressing=ipv6
IPv6LinkLocalAddressGenerationMode=stable-privacy
EOF

# Systemd-nspawn nspawn file.
my $nspawn_file_bridge = <<"EOF";
[Exec]
LimitNOFILE=10000
LinkJournal=try-host

[Network]
VirtualEthernet=yes
Bridge=br0
EOF
# ------ Local Files End --------

# ------ Subroutines Start ------
# Check distro and get current version.
sub get_release_version {
	my $version;

	open(my $file_os_release, '<:encoding(UTF-8)', "/etc/os-release")
		or die "ERROR: Unable to open /etc/os-release to determine current release version.\n";

	while(my $line = <$file_os_release>) {
		chomp $line;
		my @line_split = split(/=/, $line);

		if($line_split[0] eq "NAME" && $line_split[1] !~ /Fedora.*/i) {
			die "ERROR: Not running on Fedora.\n";
		}

		if($line_split[0] eq "VERSION_ID") {
			$version = $line_split[1];
		}
	}

	close $file_os_release;
	return $version;
}

# Check if a subvolume for the container exists.
sub btrfs_subvol_exists {
	my ($base, $subvol) = @_;

	say "INFO: Checking to see if subvolume \"$base/$subvol\" exists.";
	say "INFO: btrfs subvolume list $base";

	my @subvol_list = qx/btrfs subvolume list $base/;
	die "ERROR: Died checking if btrfs subvolume exists.\n" if($?);

	foreach my $i (@subvol_list) {
		chomp $i;
		if($i =~ /.*$subvol$/) {
			return 1;
		}
	}
	return 0;
}

# Create the btrfs subvolume to house the container.
sub btrfs_subvol_create {
	my $path = shift @_;
	my @cmd = ('btrfs', 'subvolume', 'create', $path);

	say "INFO: Creating the btrfs subvolume \"$path\".";
	say "@cmd";

	return system(@cmd);
}

# Set a quota on a subvolume if a quota doesn't exist of if the current quota
# is less then the requested size.
sub btrfs_check_quota {
	my ($base, $subvol, $quota) = @_;

	say "INFO: Checking quota on \"$base/$subvol\".";
	say "INFO: Getting subvol ID.";
	say "INFO: btrfs subvolume list $base";

	my @subvol_list = qx/btrfs subvolume list $base/;
	die "ERROR: Died checking btrfs subvolume quota.\n" if($?);

	my $subvol_id = 0;

	foreach my $i (@subvol_list) {
		chomp $i;
		if($i =~ /.*$subvol$/) {
			my @i_split = split(/ /, $i);
			$subvol_id = $i_split[1];
			last;
		}
	}

	if(!$subvol_id) {
		say "WARN: Subvolume doesn't exist.";
		return 1;
	}

	say "INFO: Checking for a current quota.";
	say "INFO: btrfs qgroup show -r $base";

	@subvol_list = qx/btrfs qgroup show -r $base/;
	die "ERROR: Died listing the btrfs qgroups.\n" if($?);

	my $subvol_limit = '';

	foreach my $i (@subvol_list) {
		chomp $i;
		if($i =~ /[0-9]+\/$subvol_id/) {
			my @i_split = split(/ +/, $i);
			$subvol_limit = $i_split[-1];
			last;
		}
	}

	if($subvol_limit) {
		say "INFO: Subvolume \"$base/$subvol\" does not have a limit.";
	}

	if($subvol_limit || parse_bytes($quota) >= parse_bytes($subvol_limit)) {
		say "INFO: Quota needs to be set on \"$base/$subvol\". Current: $subvol_limit, Requested: $quota";
		return 0;
	}

	return 1;
}

# Set the disk quota on the subvolume.
sub btrfs_subvol_set_quota {
	my ($path, $size) = @_;
	my @cmd = ('btrfs', 'qgroup', 'limit', $size, $path);
	say "INFO: Setting a $size quota on \"$path\".";
	say "@cmd";
	return system(@cmd);
}

# Install Fedora into the subvolume.
sub fedora_install {
	my ($version, $installroot) = @_;
	my @cmd = (
		"dnf",
		"-y",
		"--releasever=$version",
		"--installroot=$installroot",
		"--setopt=install_weak_deps=False",
		"install"
		);
	push(@cmd, "fedora-release");
	push(@cmd, "glibc");
	push(@cmd, "glibc-langpack-en");
	push(@cmd, "systemd");
	push(@cmd, "systemd-networkd");
	push(@cmd, "systemd-resolved");
	push(@cmd, "dbus");
	push(@cmd, "dnf");
	push(@cmd, "passwd");
	push(@cmd, "sudo");
	push(@cmd, "iproute");
	push(@cmd, "iputils");
	push(@cmd, "nftables");
	push(@cmd, "openssh-server");
	push(@cmd, "vim-default-editor");
	push(@cmd, "python3-dnf-plugin-kickstart");

	say "INFO: Installing Fedora $version at \"$installroot\".";
	say "@cmd";

	return system(@cmd);
}

# Writes content to files.
sub file_create {
	my ($file_path, $content) = @_;

	open(my $fh, ">:encoding(UTF-8)", $file_path)
		or die "ERROR: Unable to open $file_path. $!";

	print $fh $content;

	close($fh);
}
# ------ Subroutines End --------

GetOptions(
	'help' => \$help,
	'man' => \$man,
	'temp' => \$ephemeral,
	'subvol-base-path=s' => \$container_path,
	'clone=s' => \$clone,
	'release=i' => \$release_version,
	'storage-limit=s' => \$disk_size,
)
	or pod2usage(-exitval => 1, -verbose => 0);

pod2usage(-exitval => 0, -verbose => 1) if($help);
pod2usage(-exitval => 0, -verbose => 2) if($man);
die "ERROR: Container name(s) not provided.\n" if(@ARGV == 0);

if($release_version == 0) {
	$release_version = get_release_version();
}

# Check to see if the PID is 0 (root), and exit if not.
die "ERROR: Script needs to run as root.\n" if($>);

foreach my $server (@ARGV) {
	if(btrfs_subvol_exists($container_path, $server)) {
		say "INFO: Subvolume exists '$container_path/$server'";
		if(btrfs_check_quota($container_path, $server, $disk_size)) {
			if(btrfs_subvol_set_quota("$container_path/$server", $disk_size)) {
				die "ERROR: Unable to set the disk space limit on \"$container_path/$server\" subvolume.\n";
			}
		}
	} else {
		if(btrfs_subvol_create("$container_path/$server")) {
			die "ERROR: Unable to create \"$container_path/$server\" subvolume.\n";
		}
		if(btrfs_subvol_set_quota("$container_path/$server", $disk_size)) {
			die "ERROR: Unable to set the disk space limit on \"$container_path/$server\" subvolume.\n";
		}
	}

	if(fedora_install($release_version, "$container_path/$server")) {
		die "ERROR: Installing Fedora $release_version into \"$container_path/$server\".\n";
	}

	file_create("$container_path/$server/$network_file_path/$network_file_name", $network_file);
	say "INFO: Created the systemd-networkd network file.";
	file_create("$nspawn_file_path/$server.nspawn", $nspawn_file_bridge);
	say "INFO: Created the systemd-nspawn nspawn file.";
	say "TODO: Run a script/kickstart file in the container to set things up.";
	say "systemd-nspawn -D $container_path/$server"
	say "TODO: Create user and set password";
	say "TODO: Enable systemd-networkd systemd-resolved sshd inside container.";
	say "systemctl -M $server enable sshd systemd-neworkd systemd-resolved systemd-homed";
}


__END__

=head1 NAME

mkfedora.pl - Creates Fedora nspawn containers.

mkfedora.pl creates identical systemd-nspawn containers using btrfs subvolumes. By default, it creates new system containers from scratch, but it can be instructed to create clones or ephemeral containers.

=head1 SYNOPSIS

mkfedora.pl [options] name name name

--help
--man
--temp
--release N
--subvol-base-path /dir/for/subvolume (Default: /var/lib/machines)
--storage-limit N.N[T,G,M,]

To Be Implemented:

--clone /path/to/existing/container-subvolume
--mem-low N.N[T,G,M,K]
--mem-high N.N[T,G,M,K]
--mem-max N.N[T,G,M,K]
--swap-max N.N[T,G,M,K]
--cpu-percent NN
--iops-device /dev/device
--iops-read N[K]
--iops-write N[K]
--kickstart /path/to/ks.cfg

=cut
