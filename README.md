# virtscripts

Scripts to create systems in various virtualization solutions.

## Containers
### nspawn
#### mkfedora.pl

Creates a Fedora systemd-nspawn container.

  * Dependencies:
    * Perl >= 5.32
      * Number::Bytes::Human

